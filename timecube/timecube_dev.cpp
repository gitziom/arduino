#include <SimpleTimer.h>

const int redPin = 11;
const int greenPin = 10;
const int bluePin = 9;
const int button1Pin = 2;
const int button2Pin = 3;

#define BT_PRESSED LOW
#define MENU_DELAY 4
#define FADING_DELAY 20

int set = 0; //helper value to change stage
int program = 0;
// 0 - No preset
// 1 - 15 min preset
// 2 - 30 min preset
// 3 - 60 min preset

// the timer object
SimpleTimer timer;
int timer_int;
int timer_stage; //1 - green, 2 - yellow, 3 - red
long timer_value[3]; //this is program length
 
//uncomment this line if using a Common Anode LED
#define COMMON_ANODE
 
void setup()
{
  Serial.begin(9600);
  pinMode(redPin, OUTPUT);
  pinMode(greenPin, OUTPUT);
  pinMode(bluePin, OUTPUT);  
  pinMode(button1Pin, INPUT_PULLUP);
  pinMode(button2Pin, INPUT_PULLUP);
  setColor(20, 20, 10); 

  timer_value[0] = 15; // 15 * 60s = 15 min   <- changed to 1 min for test
  timer_value[1] = 30 * 60; // 30 * 60s = 30 min
  timer_value[2] = 60 * 60; // 60 * 60s = 60 min
}


void loop()
{
  timer.run();
  //Set preset BUTTON
  if (digitalRead(button1Pin) == BT_PRESSED) {
   //When btn is pressed
   //and program is running 
  if(program!=0) 
    {
      //kill timer
      timer.deleteTimer(timer_int);
      timer_stage = 0;  
      program = 0;
    }
    //cycling through colors on bt press
    if(set==3) set = 1;
    else set++;
    set_program();
    delay(500); 
    
  } 
  
  else
  //Reset cycle button
  if (digitalRead(button2Pin) == BT_PRESSED) {
   delay(500);
  } 
}


//set program
void set_program(){
    //set program color when button1 is pressed  
    switch(set)
    {
      case 1: setColor(185,5,198); break; // purple 15 min
      case 2: setColor(0,0,255); break; // blue 30 min
      case 3: setColor(255,0,0); break; // red 60 min
    }
    // then if there's a program running, reset TIMER 
    if(timer.isEnabled(timer_int))
    {
      Serial.print("Timer is enabled. Restarting");
      timer.restartTimer(timer_int);
      
    }
    else
    { //once timer is killed, let's move on to program 
      set_menuTimer(MENU_DELAY);
    }
}

//If program is running, delete timer, reset PROGRAM 
void reset_program()
{
  if(program!=0) 
    {
      timer.deleteTimer(timer_int);
      timer_stage = 0;  
      program=0;
      set=0;
     }
}



//Start any program
void start_program_with_green()
{
  if(set!=0)
  {
    reset_program();
    program = set;
    Serial.print("[DBG] program: ");
    Serial.print(program);
    Serial.print("\n");
    set_logicTimer(timer_value[program-1]*0.5);
    //Go green
    setColor(0,255,0);
    set=0;
  }
}


void set_logicTimer(long timerDelay)
{
  timer_int = timer.setTimeout(timerDelay * 1000, programEnd);
}

void set_menuTimer(long timerDelay)
{
  timer_int = timer.setTimeout(timerDelay * 1000, start_program_with_green);
}


void setColor(int red, int green, int blue)
{
  #ifdef COMMON_ANODE
    red = 255 - red;
    green = 255 - green;
    blue = 255 - blue;
  #endif
  analogWrite(redPin, red);
  analogWrite(greenPin, green);
  analogWrite(bluePin, blue);  
}

void animationGY()
{
  int i=0,j=255;
  for(i=0;i<256;i++)
  {
    setColor(i, j, 0); 
    delay(FADING_DELAY);
    if(j>100) j--;
  }
}

void animationYR()
{
  int i=0,j=255;
  for(i=100;i>0;i--)
  {
    setColor(255, i, 0); 
    delay(FADING_DELAY+10);
  }
}

//Blinking finish animation
void animationFINISH()
{
  int i=0;
  for(i=0;i<10;i++)
  {
    setColor(0, 0, 50); 
    delay(100);
    setColor(255, 0, 0); 
    delay(100);
  }
  setColor(20, 20, 10); 
}


void programEnd() {
  Serial.print("[DBG] program: ");
  Serial.print(program);
  Serial.print("\n");
  if(program>0)
  {
    Serial.print("[DBG] timer_stage: ");
    Serial.print(timer_stage);
    Serial.print("\n");
    if(timer_stage<2)
    {
      long tmpTime = 0;
      if(timer_stage==0)
      {
        animationGY();
        tmpTime = timer_value[program-1]*0.4;
      }
      else if(timer_stage==1)
      {
        animationYR();
        tmpTime = timer_value[program-1]*0.1;
      }
      timer_stage++;
      set_logicTimer(tmpTime);
    }
    //If it is last program stage, then wait for timer and
    else
    {
      Serial.print("I'm gonna finish now");
     animationFINISH();
     //We're gonna reset all (program, stage, state etc) so need to save program for later
     int tmpProgram = program;
     reset_program();
     set = tmpProgram;
     set_program();
    }
  }
}





